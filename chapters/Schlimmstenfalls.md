Schlimmstenfalls
================

\Beitext{Svite}

Sie kabbelten sich, und zwar ganz anders als die kabbelige
See herumkabbelte. Als Svite Jaschka erblickt hatte, waren
sie aufeinander zugestürmt, gegeneinander gerannt, und wälzten
sich nun auf dem Boden herum, bis Svite gewann. Svite gewann
immer. Aus der Puste lag Jaschka unter ihr. Wunderschön. Mit
diesem gespielt verzweifelten Gesichtsausdruck, der sehr
niedlich war.

"Ich möchte dich küssen.", sagte Svite.

Wo kam das denn jetzt her, dachte sie für sich.

Jaschka schüttelte den Kopf. "Nein." Und fügte, vielleicht
ängstlich, hinzu: "Bist du enttäuscht?"

Svite schüttelte den Kopf. "Dann küsse ich dich halt
nicht."

"Ich bin nicht verliebt in dich oder sowas.", erklärte Jaschka.

"Ich in dich auch nicht.", sagte Svite nachdenklich. Woher
kam der Wunsch?, fragte sie sich erneut. "Wenn ich dich angucke, dann
ist das nicht sexuell oder sowas. Und auch nicht romantisch, denke
ich. Ich mag dich einfach gern anfassen. Ich mag dein festes Haar, und
wie es riecht. Ich mag deine niedlichen Gesichtsausdrücke und dein zartes
Wimmern. Und ich mag deine Haut. Die Konsistenz davon. Ich mag
dein Gewebe."

Jaschka kicherte los -- was die Gesichtszüge zu einem nicht weniger
niedlichen Ausdruck verformte.

Es beruhigte und verwirrte Svite. Es beruhigte, weil Svite plötzlich
Angst gehabt hatte, Jaschka zu nahe getreten zu sein. Es verwirrte, weil
Svite nicht die geringste Ahnung hatte, was daran lustig sein sollte. Also
fragte sie: "Habe ich was Witziges gesagt? Und falls ja, was?"

"Ich glaube, über Konsistenz von Gewebe zu reden, ist äußerst unübliche
Worte bei Komplimenten.", hielt Jaschka breit grinsend fest.

"Merkwürdig.", sagte Svite. "Wie soll ich denn bitte sonst ausdrücken,
warum du dich für mich so gut anfühlst?" Und dann besann sie sich, und
brachte ihre Angst von vorhin noch einmal zur Sprache, damit es
sicher abgeklärt wäre. "Soll ich dich loslassen? Ist dir meine
Komplimentiererei unangenehm?"

Jaschka schüttelte den Kopf. "Ich mag dich. Ich fühle halt einfach
keine Verliebtheit oder sowas. Ich mag, dass du stärker bist. Ich
fühle mich von dir eingeklemmt irgendwie beschützt. Und wenn es wirklich
nicht romantisch ist, dann darfst du mich küssen."

"Sicher?", fragte Svite. Eigentlich neigte Jaschka nicht zu voreiligen
Entscheidungen, aber hier wollte Svite lieber noch einmal nachfragen.

Jaschka grinste. "Mach!"

"Ich kann nicht versprechen, dass ich dann nicht doch romantisches Gedöns
spüre.", überlegte Svite. Sie hatte schließlich noch nie anderer Leute
Münder geküsst. Ihren eigenen erst recht nicht, es sei denn, es zählte,
auf Lippen herumzukauen.

"Dann hörst du auf, sobald das passiert.", sagte Jaschka.

Svite quetschte Jaschka noch etwas fester zwischen den Knien ein, was
ein wunderschönes, kurzes Fiepen hervorrief, und berührte die Lippen
dieser hervorragenden Person unter ihr mit ihren eigenen, drückte
sehr sanft das weichste an Gewebe in diesem
Gesicht etwas zusammen, und zupfte vorsichtig mit den eigenen
Lippen daran. Nur kurz. Sie bewegte sich dann ein bisschen weg
von Jaschkas Gesicht, um den Ausdruck darin zu lesen. Er wirkte etwas
weniger gespielt verzweifelt. Vielleicht gelangweilt? Was hatte
Svite auch erwartet. Es war eine merkwürdige Idee.

Sie probierte es ein zweites Mal. Immerhin war es wirklich weiches,
zartes Gewebe und sehr zarte Haut. Sie fühlte sie mit den eigenen
sensiblen Lippen nach, und berührte dann mit selbigen noch
einmal die Wangen zum Vergleich. Die Wangen hatte Svite schon
oft geküsst. Woher hatte Jaschka eigentlich gewusst, dass Svite
nicht die Wangen gemeint hatte, als sie gesagt hatte, sie wolle
Jaschka küssen?

Svite richtete sich wieder auf und blickte aus sicherem Abstand
in das Gesicht unter ihr. "Wie war es?"

Jaschka schüttelte den Kopf. "Eigentlich nicht schlimm, aber
es erinnert mich zu doll an Küssen mit Leuten, die in mich
verliebt sind, und ich mag das nicht."

Svite nickte. "Dann lasse ich das halt. Das bringt dann ja
nix."

Jaschka grinste. "Ich habe dich schon ganz schön gern."

"Ich habe noch eine andere Frage, Jaschka.", sagte Svite.

Irgendwas Schönes passierte in Jaschkas Gesicht. "Der Name
ist so gut! Ich freue mich einfach jedes Mal, wenn du
ihn für mich benutzt."

Svite nickte und grinste. Das wusste sie schon, und sie
tat es deshalb absichtlich. "Willst du, dass ich, wenn ich
über dich rede oder auch nur über dich nachdenke, dass ich das mit Wörtern
wie 'sie' mache, oder sowas sage, wie
'meine Liebe', oder dass ich von dir als 'Freundin' rede?"

Jaschka nickte langsam. "Das hatte ich dir damals auch
umgekehrt angeboten. Ich glaube, das kann helfen, mich voll
in die Rolle einzufühlen. Ich denke testweise von mir selber
auch schon als 'sie'."

Svite rollte sich von Jaschka herunter und legte den eigenen
Kopf auf Jaschkas Arm. Jaschka strich Svite sehr zart über
das Haar. Svite mochte diese sehr weichen Berührungen, die
eigentlich nichtmal ihre Kopfhaut berührten, sondern nur
das Haar darauf bewegten.

Es war nun sechs Jahre her, dass Jaschka und sie von dem
Fischereitörn zurückgekehrt waren, für den Svite sich als
Junge verkleidet hatte, um mitzusegeln. Es war arg geflährlich
gewesen. Aber auch schön. Aber auch gefährlich. Aber irgendwie
hatte sich Svite in der Crew richtig gefühlt. Sie vermisste
das Gefühl. Auch wenn es so gefährlich gewesen war und es
ihr -- im Nachhinein wurde ihr das erst klar -- wohl das
Leben gekostet hätte, wenn sie entdeckt worden wäre.

Nun hatte Svite einiges an Oberweite. Ihr Körper eignete sich
für solche Späße nicht mehr. Muskeln hatte sie, nicht
wenig. Es gab einen Grund, warum Jaschka immer verlor. Aber
sie hatte Kurven und sowas, -- fürchterlichen Dinge.

Jaschka und sie hatten den Rest ihrer Spätkindheit an
Land als Gesindel eines Bauernhofs mit Fischerei gearbeitet. Es
war stinkende Schwerstarbeit. Jaschka hatte immerhin
auf eine Jungenschule gehen können. Ihr Körper machte
Schwerstarbeit schlechter mit als Svites. Eigentlich war der Plan
gewesen, dass Jaschka ihr abends zum Einschlafen das
Gelernte vermittelte, aber das scheiterte an ein paar
Dingen. An Müdigkeit, oder daran, dass Svite eine
Lernbehinderung hatte, auf die Jaschka nicht so
sonderlich brauchbar eingehen konnte. Für Svite
war Ungeduld äußerst unangenehm. Davon hatte es
bei dieser Fischerei- und Bauersarbeit auch ohne Jaschkas
Lerngedöns schon genug gegeben.

Vor einem halben Jahr hatte Svite davon die Nase voll gehabt und in
Geesthaven eine Arbeit in einer Bäckerei gefunden. Und wenn
Jaschka Svite besuchte, tat sie es mit Kleidern und einer
anderen Frisur und dem wunderschönen Namen Jaschka, weil in Svites
Zimmer kein Herrenbesuch erlaubt war.

"Möchtest du mal wieder zu einer Seefahrt anheuern?", fragte
Jaschka unvermittelt.

In Svite verkrampfte sich alles.

Jaschka merkte es sofort und legte sanft die Arme um
sie. "Antworte trotzdem.", flüsterte sie.

"Ja." Svite hatte versucht zu flüstern, aber ihre Stimme
machte nicht immer, was sie wollte, deshalb war es eher die
akustische Version von etwas Viereckigem, was da aus ihrer
Kehle kam.

"Hast du schonmal vom Windsbräutigam gehört?", fragte
Jaschka.

"Sag bloß, die Crew sucht nun weniger veradelte Leute." Natürlich
hatte Svite von der reinen Frauencrew gehört, die mit einem
Schiff namens Windsbräutigam die Monarchin Skanderns durch
die Gegend kutschierte. Die Monarchin kam aus Nordskandern und besuchte
gelegentlich ihre Familie ohne ihren Gemahl. Damit das alles
andstandsmäßig seine Richtigkeit hatte, oder auch damit
sie weniger lüsterne Blicke erleiden müsste, war deshalb eine
rein weibliche Crew aus adeligen Frauen zusammengestellt
worden.

Svite machte die Existenz dieser Crew wütend, und sie wusste
nicht genau, warum.

Svite spürte ein zartes Küsschen in ihrem Haar. "Ja.", sagte
Jaschka leise.

"Möchtest du dich dort bewerben?", fragte Svite.

"Nur mit dir.", murmelte Jaschka.

"Das würde für dich genau so lebensgefährlich werden wie für mich
damals, vermute ich." Svite wunderte sich über die eigene
Stimmung. Sie vermutete, dass nicht so viel davon nach außen
drang. Das war oft so. Obwohl Jaschka durchaus sehr feinfühlig
war für so etwas. Sie hätte jedenfalls irgendwie glücklich
sein müssen: Es war da Hoffnung, dass sie wieder zur See fahren
könnte! Sie hatte geliebt, auf einem Schiff zu
leben! So eine Gelegenheit wie jetzt würde sich nicht wieder
bieten. Und vielleicht hätte sie als sehr kräftige Person mit
Erfahrung auch Chancen. Sie murmelte: "Wie erkläre ich meine
seemännischen, äh, seewesenischn Erfahrungen?"

"Wir sagen, dass wir mal als Jungen verkleidet auf einem
Fischereitörn mitgesegelt sind?", schlug Jaschka vor.

"Riskant." Svite musste trotzdem grinsen. Es war witzig. Es
traf ihren Humor. Die merkwürdige Asymmetrie von zwei
Herzpersonen, von denen die eine -- sie -- zuvor sich
verkleidet hatte, und nun wäre es umgekehrt. Und das
Verbotene von damals offen zu legen, könnte die Eintrittskarte
sein.

"Was könnte schlimmstenfalls passieren?", fragte Jaschka.

Svite grinste breiter. Sie war gut darin, Horrorgeschichten zu
erspinnen. "In jeder Variante wirst du ausgezogen."

"Ich weiß.", seufzte Jaschka freundlich. "Sei kreativ."

"Ich denke, es passiert als Folge auf deinen Stimmbruch. Oder
weil du beim Anblick einer der Frauen eine
Errektion bekommst. Aber auch nicht
direkt nach Fahrtantritt. Sondern so mitten auf dem Meer. Irgendwo, wo
es kalt ist. Am besten am Südpol.", sinnierte Svite.

"Wieso fahren wir zum Südpol? Reicht der Norden nicht?", fragte
Jaschka.

"Südpol ist weiter weg. Wir nehmen den Südpol. Aus
Sicherheitsgründen." Der Ausdruck 'Aus Sicherheitsgründen' war
etwas, was sie einfach überall dranhängte, wo es eigentlich keine
gute Begründung gab. "Sie setzen uns da auf einer Eisscholle aus."

"Uns? Wieso dich?", fragte Jaschka.

"Weil ich dich nicht allein krepieren lassen werde, du
Schnürsenkel." Svite suchte, ohne hinzusehen, mit dem
Zeigefinger Jaschkas Nase, um
daraufzustupsen, und stupste auf der Suche eine Augenbraue, eine
Wange und einen Mundwinkel, bevor sie die Nasenspitze fand.

"Äußerst liebenswürdig. Ich werde zusehen, dass ich mich niemals
ausziehe.", versicherte Jaschka.

"In unserer Horrorgeschichte übernehmen das andere.", korrigierte
Svite. "Es handelt sich um eine Unausweichlichkeit. Und dann erfrieren
wir auf dieser Eisscholle. Das ist es. Schlimmstenfalls."

"Wenigstens hast du noch Kleidung.", stellte Jaschka beruhigt fest.

"Die reicht nicht für uns beide. Aber du kannst sie ganz haben.", überlegte
Svite. "Du bist viel frostkötteliger als ich."

"Einverstanden." Jaschka wirkte trotzdem zögerlich, und ihr
Einwand ließ auch nicht lange auf sich warten. "Erfrieren ist, habe
ich gehört, ein verhältnismäßig freundlicher Tod."

"Oh, du hast recht.", stimmte Svite zu. "Also, dann kommen, kurz
bevor wir gefühlstaub sind, noch Eisbären. Oder Haie. Die uns
verspeisen. Langsam."

"Nun hört sich das schon eher nach einem schlimmen Szenario
an.", überlegte Jaschka. "Tun wir's?"

Svite atmete sehr tief ein und aus. Sie hatte doch längst
entschieden. Auch wenn ihr da einiges dran nicht gefiel. Etwa, dass
sie nicht die Person mit dem viel zu hohen Risiko war. Aber
das war albern. Sie wollte für sie beide kein solches Risiko. Sie
atmete noch einmal tief ein und aus. "In Ordnung. Ich bin dabei."
